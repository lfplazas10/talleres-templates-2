package taller.interfaz;

/*
 * SudokuCLI.java
 * This file is part of SudokuCLI
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * SudokuCLI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SudokuCLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SudokuCLI. If not, see <http://www.gnu.org/licenses/>.
 */


import taller.mundo.Sudoku;
import static taller.mundo.Sudoku.Rating;

import java.util.Map;
import java.util.Set;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *  La clase <tt>SudokuCLI</tt> establece un sistema de interacción directa
 *  con un usuario final, basada en el uso de un entorno de comando de línea (CLI).
 *  A través de este, un usuario puede acceder y jugar al Sudoku.
 *  @author ISIS1206 Team
 */

public class SudokuCLI
{
	/**
	 * Referencia directa a un juego de Sudoku.
	 **/
	private Sudoku sudoku;

	/**
	 * Interfaz principal al flujo estándar de entrada.
	 * Permite obtener la entrada del usuario a través de consola
	 **/
	private Scanner in;

	/**
	 * Constuctor principal de la clase
	 **/
	public SudokuCLI()
	{
		in = new Scanner(System.in);
		sudoku = new Sudoku();
	}

	/**
	 * Menú principal del juego
	 **/
	public void mainMenu(){
		boolean finish = false;
		while(!finish){
			Screen.clear();
			System.out.println("----------------");
			System.out.println("-              -");
			System.out.println("-    Sudoku    -");
			System.out.println("-              -");
			System.out.println("----------------\n");
			System.out.println("Menú principal");
			System.out.println("--------------");
			System.out.println("1. Iniciar nuevo juego");
			System.out.println("2. Salir\n");
			System.out.print("Seleccione una opción: ");
			int opt = in.nextInt();
			switch(opt){
				case 1:
					newGame();
					break;
				case 2:
					finish = true;
					break;
			}
			Screen.clear();
		}
	}

	/**
	 * Menú de selección de dificultad de una nueva partida de Sudoku
	 **/
	public void newGame(){
		sudoku.resetGame();
		System.out.print("------------------\n");
		in.nextLine();
		System.out.println("Nivel de dificultad");
		Rating[] difficultyLvl = Rating.values();
		System.out.print("------------------\n");
		for (int i = 0; i < difficultyLvl.length; i++) {
			int index = i+1;
			System.out.println(index+"."+difficultyLvl[i]);
		}
		System.out.println("6.Regresar el menu principal\n");
		System.out.print("Seleccione un nivel de dificultad: ");
		int opt = in.nextInt();

		switch(opt){
			case 1:
				sudoku.setUpGame(difficultyLvl[0]);
				startGame();
				break;
			case 2:
				sudoku.setUpGame(difficultyLvl[1]);
				startGame();
				break;
			case 3:
				sudoku.setUpGame(difficultyLvl[2]);
				startGame();
				break;
			case 4:
				sudoku.setUpGame(difficultyLvl[3]);
				startGame();
				break;
			case 5:
				sudoku.setUpGame(difficultyLvl[4]);
				startGame();
				break;
			case 6:
				return;
			default: 
				System.out.println("Por favor asegurese de ingresar una opcion correcta.");
				newGame();
		}
		/**
		 * TODO: Realice un menú de selección de dificultad, de acuerdo a los niveles
		 *       declarados y dispuestos en el arreglo difficultyLvl. Nota: Es posible
		 *       realizar la impresión de cada uno de los niveles, a partir de la invocación
		 *       de la función toString(). Adicionalmente, es necesario presentar una opción 
		 *       que permita al usuario retornar al menú principal. Para este fin, es suficiente
		 *       permitir que el método actual finalice sin ejecutar instrucciones adicionales
		 **/
	}

	/**
	 * Ciclo principal de ejecución del juego de Sudoku
	 **/
	public void startGame(){
		/**
		 * TODO: Construya la interfaz de interacción principal con la partida en curso.
		 *       Esta interfaz debe, en primer lugar, visualizar el estado actual del
		 *       tablero de juego (Ver printBoard). En segundo lugar, debe ofrecer un menú
		 *       bajo el cual, el usuario pueda realizar las siguientes acciones:
		 *           - Introducir un valor en una casilla
		 *           - Eliminar el valor de una casilla
		 *           - Finalizar juego y ver solución
		 *       Finalmente, es necesario verificar el estado de finalización de juego
		 *       tras la ejecución de un cambio en el tablero (Ver Sudoku.hasGameFinished())
		 **/
		in.nextLine();
		System.out.println("------------------");
		boolean k = true;
		while (k){
			System.out.println(printBoard(sudoku.getBoard()));
			System.out.println("------------------");
			System.out.println("Sudoku Menu:");
			System.out.println("------------------");
			System.out.println("1.Introducir número en una casilla");
			System.out.println("2.Eliminar el valor de una casilla");
			System.out.println("3.Finalizar juego y ver la solución\n");
			System.out.print("Seleccione una opción: ");
			int answer = in.nextInt();
			if (answer == 1){
				enterValue();
				if (sudoku.hasGameFinished()){
					System.out.println("-------------------------");
					System.out.println("Ha finalizado el juego, ¡Felicitaciones!");
					System.out.println("-------------------------");
					in.nextLine();
					break;
				}
			}
			else if (answer == 2){
				deleteValue();
			}
			else if (answer == 3){
				System.out.println("-------------------------");
				System.out.println("Posible solución del juego");
				System.out.println("-------------------------");
				System.out.println(printBoard(sudoku.getSolution()));
				System.out.print("Presione cualquier tecla para continuar...");
				in.nextLine();
				break;
			}
			else {
				System.out.println("Por favor introduzca una opción del 1 al 3");
			}
		}
		in.nextLine();
	}

	/**
	 * Dialogo dispuesto para la sustitución de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informará al mismo a través de consola.
	 **/
	public void enterValue(){
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla, acompañada del número a introducir, e.g., A2-3\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		String[] values = coor.split("-");
		int num = Integer.parseInt(values[1]);
		int row = ((int) values[0].charAt(0)) - 65;
		int col = Integer.parseInt(""+values[0].charAt(1))-1;
		if(row < 0 || row > 8){
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else{
			if (col < 0 || col > 8){
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else{
				String result = sudoku.replaceValue(row, col, num, false);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Dialogo dispuesto para la eliminación de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informará al mismo a través de consola.
	 **/
	public void deleteValue()
	{
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla que se desea eliminar, e.g., B4\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		int row = ((int) coor.charAt(0)) - 65;
		int col = Integer.parseInt(""+coor.charAt(1))-1;
		if(row < 0 || row > 8)
		{
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else
		{
			if(col < 0 || col > 8)
			{
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else
			{
				String result = sudoku.replaceValue(row, col, 0, true);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Imprime un tablero de Sudoku en consola, a partir del uso de caractéres de construcción de tablas.
	 * @param Matriz que describe un tablero de un juego de Sudoku.
	 * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
	 * encuentra vacía. 0 de lo contrario
	 * @return Representación textual de un tablero de sudoku.
	 **/
	public String printBoard(int[][] board)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n     1   2   3   4   5   6   7   8   9\n");
		sb.append("   ┌───┬───┬───┬───┬───┬───┬───┬───┬───┐\n");
		int rowNum = 1;
		for(int[] row : board)
		{
			sb.append(" "+((char)(rowNum + 64))+" ");
			for(int col : row)
			{
				String val = (col == 0) ? " " : ""+col;
				sb.append("│ "+val+" ");
			}
			sb.append("│\n");
			if(rowNum != 9)
			{
				sb.append("   ├───┼───┼───┼───┼───┼───┼───┼───┼───┤\n");
			}
			else
			{
				sb.append("   └───┴───┴───┴───┴───┴───┴───┴───┴───┘\n");
			}
			rowNum++;
		}
		return sb.toString();
	}


}

